package com.hauoli.trackhandsimple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.hauoli.trackhand.HauoliConst;
import com.hauoli.trackhand.HauoliTracker;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //-------------------------------
        // TODO: need to get user permission to access mic


        //-------------------------------
        // Initialization
        int nDim = 2;
        int nSpk = 1;
        int nMic = 1;
        // set the initial distance from hand to mic = 100mm
        double[] initPos = {0, -100};
        // the position of the speaker on phone: [0, 0]
        double[] spkPos  = {0, 0};
        // the position of the microhpone on phone: [0, 0]
        double[] micPos  = {0, 0};
        
        HauoliTracker.initTracker(nSpk, nMic, initPos, spkPos, micPos);

        //-------------------------------
        // Other Configuration (don't need to change)
        HauoliTracker.setDistEstMethod(HauoliConst.METHOD_TAP);
        HauoliTracker.setAudioSource(HauoliConst.AUDIO_SOURCE_JAVA);
        HauoliTracker.setSeq(30, 240, 6000);
        HauoliTracker.setMinTap(10);
        HauoliTracker.setMaxTap(250);
        HauoliTracker.setTapStep(20);
        HauoliTracker.setAllowReset(false);
        HauoliTracker.setAutoThrd(HauoliConst.TAP_SMOOTH_THRD_FIX, 0.00001);
        HauoliTracker.setRecordAudio(false);
        HauoliTracker.setSaveDist(false);
        HauoliTracker.setUseFile(false);
        HauoliTracker.enableGesture(true);

        //-------------------------------
        // Start tracking
        double[] dists = new double[nMic];

        HauoliTracker.start();
        while(true) {
            int state = HauoliTracker.getState();
            if(state != HauoliConst.TRACKER_STATE_TRACKING) {
                Log.d("Hauoli", "Calibrating: Keep Stationary");
                continue;
            }

            int gest    = HauoliTracker.getGesture();
            int gestCnt = HauoliTracker.getGestCnt();
            HauoliTracker.getDists(dists);
            Log.d("Hauoli", String.format("gesture = %d (%d), est dist = %f", gest, gestCnt, dists[0]));

            try {
                Thread.sleep(10);
            } catch (Exception e) {
                Log.e("Error", "run: " + e.toString());
            }
        }

        // HauoliTracker.stop();
    }
}
