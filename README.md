# Hauoli Acoustic-based Motion Tracking SDK

## Overview

The Hauoli Tracking system provides accurate motion tracking technology. It has several unique advantages over existing motion tracking: (I) achieves mm-level accuracy, (II) no need for special hardware or external infrastructure and it works as a smartphone app, and (III) works under obstruction and different lighting conditions. The number of potential applications of Hauoli motion tracking is endless, from switching a TV channel to controlling an air conditioner by waving your hand, from playing more fine-grained motion games like first person shooting to providing immersive experience in VR/AR, or even controlling drones.


## Hint

1. Copy Hauoli Hand Tracking SDK to the following directory:

   - XCode project for iOS:
   
   ```
   libs/libHauoliTrack.a
   libs/HauoliConst.h
   libs/HauoliTracker.h
   ```

   - Android Studio project for Android:

   ```
   app/lib/HauoliTrackHand.aar
   ```

2. Android OpenCV SDK is required.

   - iOS:

   ```
   https://docs.opencv.org/2.4/doc/tutorials/introduction/ios_install/ios_install.html
   ```   

   There is a local version located in the sample project at:

   ```
   opencv2.framework
   ```

   - Android:

   ```
   https://opencv.org/android/ 
   ```
   
   There is a local version located in the sample project at:

   ```
   app/lib/openCVLibrary300-release
   ```

3. For [iOS sample project](https://gitlab.com/yichao0319/trackhandsimpleios), the project can be imported to __XCode__.

   For [Android sample project](https://gitlab.com/yichao0319/hauolitrackhandsimple) project: the project has been imported and tested in [__Android Studio Preview__](https://developer.android.com/studio/preview/?gclid=CjwKCAjwq-TmBRBdEiwAaO1eny8vexq7ej-vAx6Nhl1IedBPua0cFctvyztO1B-leuxWfC3GB-O-xhoCFaMQAvD_BwE). The project can be also imported to standard __Android Studio__ but needs to modify the configuration accordingly.

4. The permission to access the microphone and storage is required. Android Developers need to wirte the codes to get permission or let Andorid users manually grant the permission in Android __Settings__. iOS Developers need to grant file sharing permission (i.e., set "Application supports iTunes file sharing" to "YES").

5. After running the APP, one can see the estimated distances with __adb logcat__ (Android) or __Console View__ (XCode).


## SDK

### Android SDK

- __HauoliTrackHand.aar__

### iOS SDK

- __libHauoliTrack.a__: the Hauoli Tracking Objective-C library.

- __HauoliTracker.h__: the header file for Hauoli Tracking library.

- __HauoliConst.h__: definition of constant used in the library.


## APIs

- int initTracker(int numSpk, int numMic, double[] initPos, double[] spkPos, double[] micPos)

   | Input  | Description       |
   | ------ | ------------------|
   | numSpk | Number of speakers on phone |
   | numMic | Number of mics on phone |
   | initPos | The initial coordinate of the target (unit: mm) |
   | spkPos | The coordinate of the speakers (unit: mm) |
   | micPos | The coordinate of the mics (unit: mm) |

- int start()

    Start tracking.

- int stop()

    Stop tracking.

- void getDists(double[] dists)

   | Input  | Description       |
   | ------ | ------------------|
   | dists  | Output the distances to each mic |

- double getDist()

    Return the distance to the first mic.

- double \*getDistsHist()

    Get the history of estimated distances since the last time it's called.

    The 1st element in the returned vector indicates the number of distances in the vector. That is, ret[0] = 10 means there are 10 distances returned and the vector length is 11.

    The longest distance history the API keeps is 0.1s.

    Android example codes:

    ```
    double[] dists_hist = HauoliTracker.getDistsHist();
    if(dists_hist != null) {
        int len = (int)dists_hist[0];
        for(int i = 0; i < len; i ++) {
            Log.d("Hauoli", String.format("dist hist %d/%d: %f", i, len, dists_hist[i+1]));
        }
    }
    ```

    iOS example codes:

    ```
    double *dist_hist = [hauoliTracker getDistsHist];
    if(dist_hist != NULL) {
        int len = (int)dist_hist[0];
        for(int i = 0; i < len; i ++) {
            NSLog(@"%d/%d: %f, %f", i, len, dist_hist[i+1]);
        }
    }
    ```

- void getPos(double[] pos)
   
   | Input  | Description       |
   | ------ | ------------------|
   | pos | Output the coordinate of the target |

- double getPosX()

    Return the x-axis of the target's coordinate.

- double getPosY()

    Return the y-axis of the target's coordinate.

- double getPosZ()

    Return the z-axis of the target's coordinate.

- int getState()

    Return the tracking state:

    ```
    public static final byte TRACKER_STATE_STOP        = 1;
    public static final byte TRACKER_STATE_SYNC        = 2;
    public static final byte TRACKER_STATE_SKEW        = 3;
    public static final byte TRACKER_STATE_CANCEL      = 4;
    public static final byte TRACKER_STATE_CALI        = 5;
    public static final byte TRACKER_STATE_TRACKING    = 6;
    ```

- double getPower()

    Return the audio magnitude. When the magnitude becomes too low, the tracking may become less accurate.

- int getB()

    Return the bandwidth of the acoustic signals used for tracking.

- void getFc(int[] f)

    Return the central frequencies of the acoustic signals used for each speaker.

- boolean setDIstEstMethod(int method)

- boolean setAudioSource(int src)

    ```
    public static final byte AUDIO_SOURCE_SP     = 0;
    public static final byte AUDIO_SOURCE_AAUDIO = 1;
    public static final byte AUDIO_SOURCE_JAVA   = 3;
    public static final byte AUDIO_SOURCE_USER   = 4;
    ```

- boolean setB(int b)

- boolean setFc(int[] f)

- boolean setUseFile(boolean if_file)

- boolean setRecordAudio(boolean if_record)

    Set to true to record audio while tracking. 

    In Android, the file is recorded save

    ```
    /sdcard/Hauoli/[TIMESTAMP].wav
    ```

    In iOS, the file is recorded save

    ```
    Documents/[TIMESTAMP].wav
    ```

- boolean setSaveDist(boolean if_save)

    Set to true to save the estimated distances while tracking. 

    In Android, the file is recorded save

    ```
    /sdcard/Hauoli/[TIMESTAMP].txt
    ```

    In iOS, the file is recorded save

    ```
    Documents/[TIMESTAMP].txt
    ```

- boolean setAudioSource(int src)

    ```
    public static final byte AUDIO_SOURCE_SP     = 0;
    public static final byte AUDIO_SOURCE_AAUDIO = 1;
    public static final byte AUDIO_SOURCE_JAVA   = 3;
    public static final byte AUDIO_SOURCE_USER   = 4;
    ```

- boolean setAllowReset(boolean allowed)

- boolean setAutoThrd(int type, double thrd)

    Methods to control the thresholding of Hauoli Tracker:

    - Based on the mangitude of phase changes:

      a) type == TAP_MAG_THRD_FIX
         Use the fixed threshold "thrd".

      b) type == TAP_MAG_THRD_CALI
         Perform the calibration at the beginning of the tracking to determine the threshold.

      c) type == TAP_MAG_THRD_REC
         Use the threshold from the previous calibration results. For the first two times when the App is used, we conduct the calibration and see if the thresholds are similar. If they are, the threshold is used afterward. Otherwise, we keep conduct the calibration untill the last two thresholds are similar.

    - Based on the moothness of the estimated distance:

      a) type == TAP_SMOOTH_THRD_FIX
         Use the fixed threshold "thrd".

    ```
    public static final byte TAP_MAG_THRD_FIX          = 0;
    public static final byte TAP_MAG_THRD_CALI         = 1;
    public static final byte TAP_MAG_THRD_REC          = 2;
    public static final byte TAP_MAG_THRD_REC_RESET    = 3;
    public static final byte TAP_SMOOTH_THRD_FIX       = 4;
    public static final byte TAP_SMOOTH_THRD_CALI      = 5;
    public static final byte TAP_SMOOTH_THRD_REC       = 6;
    public static final byte TAP_SMOOTH_THRD_REC_RESET = 7;
    ```

- boolean pushAudio(byte[] y, int byte_per_elem, int num_elem)

- boolean setSkipTime(double time)

- int getNSeq()

- int getNSeqUp()

- int getMinTap()

- int getMaxTap()

- int getTapStep()

- void getCirDiff(double[] cir_dif_real, double[] cir_dif_imag)

- void getPlayBuffer(short[] buf)

- boolean setSeq(int num_seq, int num_seq_up, int b)

- boolean setNSeq(int num_seq)

- boolean setNSeqUp(int num_seq_up)

- boolean setMinTap(int min_tap)

- boolean setMaxTap(int max_tap)

- boolean setTapStep(int tap_step)

- int getGesture()

    ```
    public static final byte GESTURE_NONE         = 0;
    public static final byte GESTURE_TICKLE       = 1;
    public static final byte GESTURE_HIT          = 2;
    public static final byte GESTURE_PAT          = 3;
    public static final byte GESTURE_AWAY         = 4;
    public static final byte GESTURE_TOWARD       = 5;
    public static final byte GESTURE_1CLICK_LONG  = 6;
    public static final byte GESTURE_1CLICK_SHORT = 7;
    public static final byte GESTURE_2CLICK_LONG  = 8;
    public static final byte GESTURE_2CLICK_SHORT = 9;
    public static final byte GESTURE_3CLICK_LONG  = 10;
    public static final byte GESTURE_3CLICK_SHORT = 11;
    public static final byte GESTURE_4CLICK_LONG  = 12;
    public static final byte GESTURE_4CLICK_SHORT = 13;
    ```

- int getGestCnt()
  
    The counter of the performed gestures.

- boolean trajCleaned()

- void setRecordingDeviceId(int id)

- void setPlaybackDeviceId(int id)


## Sample Codes

### Android

```
import com.hauoli.trackhand.HauoliConst;
import com.hauoli.trackhand.HauoliTracker;

/***********************
 * Initialization
 ***********************/
int nDim = 2;
int nSpk = 1;
int nMic = 1;
// set the initial distance from hand to mic = 100mm
double[] initPos = {0, -100};
// the position of the speaker on phone: [0, 0]
double[] spkPos  = {0, 0};
// the position of the microhpone on phone: [0, 0]
double[] micPos  = {0, 0};

HauoliTracker.initTracker(nSpk, nMic, initPos, spkPos, micPos);

/***********************
 * Other Configuration (don't need to change)
 ***********************/
HauoliTracker.setDistEstMethod(HauoliConst.METHOD_TAP);
HauoliTracker.setAudioSource(HauoliConst.AUDIO_SOURCE_JAVA);
HauoliTracker.setSeq(30, 240, 6000);
HauoliTracker.setMinTap(10);
HauoliTracker.setMaxTap(250);
HauoliTracker.setTapStep(20);
HauoliTracker.setAllowReset(false);
HauoliTracker.setAutoThrd(HauoliConst.TAP_SMOOTH_THRD_FIX, 0.00001);
HauoliTracker.setRecordAudio(false);
HauoliTracker.setSaveDist(false);
HauoliTracker.setUseFile(false);
HauoliTracker.enableGesture(false);

/***********************
 * Start tracking
 ***********************/
double[] dists = new double[nMic];

HauoliTracker.start();
while(true) {
HauoliTracker.getDists(dists);
Log.d("Hauoli", "est dist = " + dists[0]);

try {
    Thread.sleep(10);
} catch (Exception e) {
    Log.e("Error", "run: " + e.toString());
}
}

/***********************
 * Stop tracking
 ***********************/
HauoliTracker.stop();
```

### iOS (Objective-C)

```
#import "HauoliConst.h"
#import "HauoliTracker.h"

/***********************
 * Initialization
 ***********************/
int nMic = 1;
int nSpk = 1;
int nDim = 2;
double *initPos = (double *)malloc(sizeof(double)*nDim);
double *spkPos  = (double *)malloc(sizeof(double)*nSpk*nDim);
double *micPos  = (double *)malloc(sizeof(double)*nMic*nDim);

// set the initial distance from hand to mic = 100mm
initPos[0] = 0;
initPos[1] = -100; 
// the position of the speaker on iPhone: [0, 0]
spkPos[0] = 0;    
spkPos[1] = 0;
// the position of the microhpone on iPhone: [0, 0]
micPos[0] = 0; 
micPos[1] = 0;

HauoliTracker *hauoliTracker = [[HauoliTracker alloc] init];
[hauoliTracker initTracker:nSpk num_mic:nMic init_pos:initPos spk_pos:spkPos mic_pos:micPos];

/***********************
 * Other Configuration (don't need to change)
 ***********************/
[hauoliTracker setDistEstMethod:METHOD_TAP];
[hauoliTracker setAudioSource:AUDIO_SOURCE_USER];
[hauoliTracker setSeq:30 num_seq_up:240 b:6000];
[hauoliTracker setMinTap:10];
[hauoliTracker setMaxTap:250];
[hauoliTracker setTapStep:20];
[hauoliTracker setAllowReset:false];
[hauoliTracker setAutoThrd:TAP_SMOOTH_THRD_FIX thrd:0.00001];
[hauoliTracker setRecordAudio:false];
[hauoliTracker setSaveDist:false];
[hauoliTracker setUseFile:false];
[hauoliTracker enableGesture:false];

/***********************
 * Start tracking
 ***********************/
double *estDists = (double *)malloc(sizeof(double)*nMic);

[hauoliTracker start];
while(true) {
    [hauoliTracker getDists:estDists];
    NSLog(@"dist = %f", estDists[0]);

    [NSThread sleepForTimeInterval:0.05];
}

/***********************
 * Stop tracking
 ***********************/
[hauoliTracker stop];
```

